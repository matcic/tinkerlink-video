import { Module } from '@nestjs/common';
import { VideoModule } from './modules/video/video.module';

@Module({
  imports: [VideoModule],
})
export class AppModule {}
