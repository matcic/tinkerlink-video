import { createConnection } from 'typeorm';
import { Video } from '../modules/video/video.entity';
import { DB_CONNECTION_TOKEN } from '../constants';

export const dbProvider = {
  provide: DB_CONNECTION_TOKEN,
  useFactory: async () => {
    const connection = await createConnection({
      type: 'postgres',
      host: process.env.POSTGRES_HOST,
      port: parseInt(process.env.POSTGRES_PORT, 10) || 5432,
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB,
      entities: [Video],
      migrations: ['migrations/*.ts'],
    });
    await connection.runMigrations();
    return connection;
  },
};
