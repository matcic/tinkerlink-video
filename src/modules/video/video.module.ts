import { Module } from '@nestjs/common';
import { DBModule } from '../../db/db.module';
import { VideoController } from './video.controller';
import { videoProviders } from './video.providers';
import { VideoService } from './video.service';

@Module({
  imports: [DBModule],
  controllers: [VideoController],
  providers: [...videoProviders, VideoService],
})
export class VideoModule {}
