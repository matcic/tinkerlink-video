import { Test, TestingModule } from '@nestjs/testing';
import { VideoController } from './video.controller';
import { VideoService } from './video.service';
import { Video } from './video.entity';
import { videoProviders } from './video.providers';
import { dbProvider } from './../../db/db.providers';
import { transformAndValidate } from 'class-transformer-validator';
import { NotFoundException, BadRequestException } from '@nestjs/common';
import { CreateVideoDto } from './dto/create-video.dto';

describe('Video Controller', () => {
  let module: TestingModule;
  let controller: VideoController;
  let service: VideoService;

  const date = new Date();
  const video1 = {
    id: 1,
    url: 'http://www.google.com',
    description: 'Google',
    createdAt: date,
    updatedAt: date,
  };
  const video2 = {
    id: 2,
    url: 'http://www.youtube.com',
    description: 'YouTube',
    createdAt: date,
    updatedAt: date,
  };

  const videos: Video[] = [video1, video2];

  beforeAll(async () => {
    module = await Test.createTestingModule({
      providers: [VideoService, ...videoProviders, dbProvider],
      controllers: [VideoController],
    }).compile();

    controller = module.get<VideoController>(VideoController);
    service = module.get<VideoService>(VideoService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an array of videos', async () => {
      jest.spyOn(service, 'findAll').mockImplementation(() => videos);
      expect(await controller.findAll()).toBe(videos);
    });
  });

  describe('findOne', () => {
    let findOneMock;

    beforeEach(() => {
      findOneMock = jest
        .spyOn(service, 'findOneById')
        .mockImplementation(id => videos.find(video => video.id === id));
    });

    afterEach(() => {
      jest.restoreAllMocks();
    });

    it('should return a video object', async () => {
      expect.assertions(2);
      const result = await controller.findOne(1);
      expect(findOneMock.mock.calls.length).toBe(1);
      expect(result).toBe(video1);
    });

    it('should throw a NotFoundException', async () => {
      expect.assertions(3);
      try {
        await controller.findOne(3);
      } catch (err) {
        expect(err instanceof NotFoundException).toBeTruthy();
        expect(err.message).toEqual({
          statusCode: 404,
          error: 'Not Found',
          message: 'Could not find video with id 3!',
        });
        expect(findOneMock.mock.calls.length).toBe(1);
      }
    });
  });

  describe('create', () => {
    let createManyMock;

    beforeEach(async () => {
      createManyMock = jest
        .spyOn(service, 'createMany')
        .mockImplementation(async (videoDtos: CreateVideoDto[]) => {
          await transformAndValidate(Video, videoDtos);
        });
    });

    afterEach(() => {
      jest.restoreAllMocks();
    });

    it('should create multiple videos', async () => {
      expect.assertions(2);
      const data = [{ url: 'www.google.com', description: 'Google' }];
      await controller.create(data);
      expect(createManyMock.mock.calls.length).toBe(1);
      expect(createManyMock).toBeCalledWith(
        await transformAndValidate(Video, data),
      );
    });

    it('should throw a BadRequestException (bad url)', async () => {
      const data = [{ url: 'google', description: 'Google' }];
      expect.assertions(3);
      try {
        await controller.create(data);
      } catch (err) {
        expect(err instanceof BadRequestException).toBeTruthy();
        expect(err.message).toEqual({
          statusCode: 400,
          error: 'Bad Request',
          message: [
            [
              {
                children: [],
                constraints: {
                  isUrl: 'url must be an URL address',
                },
                property: 'url',
                target: {
                  description: 'Google',
                  url: 'google',
                },
                value: 'google',
              },
            ],
          ],
        });
        expect(createManyMock.mock.calls.length).toBe(1);
      }
    });

    it('should throw a BadRequestException (bad description)', async () => {
      const data = [{ url: 'www.google.com', description: 'G' }];
      expect.assertions(3);
      try {
        await controller.create(data);
      } catch (err) {
        expect(err instanceof BadRequestException).toBeTruthy();
        expect(err.message).toEqual({
          statusCode: 400,
          error: 'Bad Request',
          message: [
            [
              {
                children: [],
                constraints: {
                  length:
                    'description must be longer than or equal to 3 characters',
                },
                property: 'description',
                target: {
                  description: 'G',
                  url: 'www.google.com',
                },
                value: 'G',
              },
            ],
          ],
        });
        expect(createManyMock.mock.calls.length).toBe(1);
      }
    });
  });
});
