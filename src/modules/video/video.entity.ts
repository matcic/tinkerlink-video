import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Length, IsUrl } from 'class-validator';

@Entity()
export class Video {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('text', { nullable: false })
  @IsUrl()
  url: string;

  @Column({ length: 500, nullable: false })
  @Length(3, 500)
  description: string;

  @CreateDateColumn({ type: 'timestamp with time zone', nullable: false })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp with time zone', nullable: false })
  updatedAt: Date;
}
