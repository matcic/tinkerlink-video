import { Connection } from 'typeorm';

import { Video } from './video.entity';
import { VIDEO_REPOSITORY_TOKEN, DB_CONNECTION_TOKEN } from '../../constants';

export const videoProviders = [
  {
    provide: VIDEO_REPOSITORY_TOKEN,
    useFactory: (connection: Connection) => connection.getRepository(Video),
    inject: [DB_CONNECTION_TOKEN],
  },
];
