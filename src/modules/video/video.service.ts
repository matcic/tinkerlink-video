import { Inject, Injectable } from '@nestjs/common';
import { transformAndValidate } from 'class-transformer-validator';
import { Repository } from 'typeorm';
import { VIDEO_REPOSITORY_TOKEN } from '../../constants';
import { CreateVideoDto } from './dto/create-video.dto';
import { Video } from './video.entity';

@Injectable()
export class VideoService {
  constructor(
    @Inject(VIDEO_REPOSITORY_TOKEN)
    private readonly videoRepository: Repository<Video>,
  ) {}

  public async findAll(): Promise<Video[]> {
    return await this.videoRepository.find();
  }

  public async createMany(videoDtos: CreateVideoDto[]): Promise<void> {
    const videos: Video[] = await transformAndValidate(Video, videoDtos);
    await this.videoRepository
      .createQueryBuilder()
      .insert()
      .into(Video)
      .values(videos)
      .execute();
  }

  public async findOneById(id: number): Promise<Video> {
    return await this.videoRepository.findOne(id);
  }
}
