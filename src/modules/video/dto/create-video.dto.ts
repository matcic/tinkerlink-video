import { IsString } from 'class-validator';

export class CreateVideoDto {
  @IsString()
  readonly description: string;

  @IsString()
  readonly url: string;
}
