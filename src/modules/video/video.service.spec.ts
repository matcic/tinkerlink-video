import { Test, TestingModule } from '@nestjs/testing';
import { VideoService } from './video.service';
import { dbProvider } from './../../db/db.providers';
import { Video } from './video.entity';
import { CreateVideoDto } from './dto/create-video.dto';
import { videoProviders } from './video.providers';
import { ValidationError } from 'class-validator';

describe('VideoService', () => {
  let service: VideoService;
  const date = new Date();
  const video1 = {
    id: 1,
    description: 'Google',
    url: 'http://www.google.com',
    createdAt: date,
    updatedAt: date,
  };
  const video2 = {
    id: 2,
    description: 'YouTube',
    url: 'http://www.youtube.com',
    createdAt: date,
    updatedAt: date,
  };

  let videos: Video[] = [video1, video2];

  const repositoryMock = {
    findOne: (id): Video => videos.find(video => video.id === id),
    find: (): Video[] => videos,
    createQueryBuilder: () => ({
      insert: () => ({
        into: _ => ({
          values: (values: Video[]) => ({
            execute: async (): Promise<void> => {
              let id = 2;
              const toBeInserted: Video[] = values.map(value => {
                id++;
                return {
                  id,
                  description: value.description,
                  url: value.url,
                  createdAt: date,
                  updatedAt: date,
                };
              });
              videos = videos.concat(toBeInserted);
            },
          }),
        }),
      }),
    }),
  };

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [VideoService, ...videoProviders, dbProvider],
    }).compile();
    service = module.get<VideoService>(VideoService);
    Object.defineProperty(service, 'videoRepository', {
      get: () => repositoryMock,
    });
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    it('should return all videos from DB', async () => {
      expect(await service.findAll()).toEqual(videos);
    });
  });

  describe('findOne', () => {
    it('should return a single video from DB', async () => {
      expect(await service.findOneById(1)).toEqual(video1);
    });
  });

  describe('createMany', () => {
    let spy;
    beforeEach(() => {
      spy = jest
        .spyOn(service, 'videoRepository' as any, 'get')
        .mockReturnValue(repositoryMock);
    });

    afterEach(() => {
      jest.restoreAllMocks();
    });

    it('should create multiple videos', async () => {
      const data: CreateVideoDto[] = [
        { url: 'http://www.example.com', description: 'Example' },
        { url: 'http://www.example2.com', description: 'Example 2' },
      ];
      await service.createMany(data);
      expect.assertions(4);
      expect(videos).toHaveLength(4);
      expect(videos).toContainEqual({
        id: 3,
        url: 'http://www.example.com',
        description: 'Example',
        createdAt: date,
        updatedAt: date,
      });
      expect(videos).toContainEqual({
        id: 4,
        url: 'http://www.example2.com',
        description: 'Example 2',
        createdAt: date,
        updatedAt: date,
      });
      expect(spy.mock.calls.length).toBe(1);
    });

    it('should throw a validation error (bad url)', async () => {
      const data = [{ url: 'google', description: 'Google' }];
      expect.assertions(5);
      try {
        await service.createMany(data);
      } catch (err) {
        expect(Array.isArray(err)).toBeTruthy();
        err.every(e => {
          expect(Array.isArray(e)).toBeTruthy();
          e.every(er => {
            expect(er instanceof ValidationError).toBeTruthy();
          });
        });
        expect(err[0]).toEqual([
          {
            children: [],
            constraints: {
              isUrl: 'url must be an URL address',
            },
            property: 'url',
            target: {
              description: 'Google',
              url: 'google',
            },
            value: 'google',
          },
        ]);
        expect(spy.mock.calls.length).toBe(0);
      }
    });

    it('should throw a BadRequestException (bad description)', async () => {
      const data = [{ url: 'www.google.com', description: 'G' }];
      expect.assertions(5);
      try {
        await service.createMany(data);
      } catch (err) {
        expect(Array.isArray(err)).toBeTruthy();
        err.every(e => {
          expect(Array.isArray(e)).toBeTruthy();
          e.every(er => {
            expect(er instanceof ValidationError).toBeTruthy();
          });
        });
        expect(err[0]).toEqual([
          {
            children: [],
            constraints: {
              length:
                'description must be longer than or equal to 3 characters',
            },
            property: 'description',
            target: {
              description: 'G',
              url: 'www.google.com',
            },
            value: 'G',
          },
        ]);
        expect(spy.mock.calls.length).toBe(0);
      }
    });
  });
});
