import {
  BadRequestException,
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  ParseIntPipe,
  Post,
} from '@nestjs/common';
import { CreateVideoDto } from './dto/create-video.dto';
import { Video } from './video.entity';
import { VideoService } from './video.service';
@Controller('videos')
export class VideoController {
  constructor(private readonly videoService: VideoService) {}

  @Post()
  public async create(
    @Body() createvideoDtos: CreateVideoDto[],
  ): Promise<void> {
    try {
      await this.videoService.createMany(createvideoDtos);
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  @Get()
  public async findAll(): Promise<Video[]> {
    return await this.videoService.findAll();
  }

  @Get(':id')
  public async findOne(@Param('id', new ParseIntPipe()) id): Promise<Video> {
    const video = await this.videoService.findOneById(id);
    if (!video) {
      throw new NotFoundException(`Could not find video with id ${id}!`);
    }
    return video;
  }
}
