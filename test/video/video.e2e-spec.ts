import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import * as request from 'supertest';
import { CreateVideoDto } from '../../src/modules/video/dto/create-video.dto';
import { VideoModule } from '../../src/modules/video/video.module';
import { classToPlain } from 'class-transformer';
import { VideoService } from '../../src/modules/video/video.service';
import { createConnection } from 'typeorm';
import { Video } from '../../src/modules/video/video.entity';
import * as fs from 'fs';

let videoRepository;
let fixtures;

const truncateVideoTable = async () => {
  const connection = await createConnection({
    name: 'test',
    type: 'postgres',
    host: process.env.POSTGRES_HOST,
    port: parseInt(process.env.POSTGRES_PORT, 10) || 5432,
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB,
    entities: [Video],
    synchronize:
      process.env.NODE_ENV === 'dev' || process.env.NODE_ENV === 'test',
  });

  videoRepository = connection.getRepository(Video);
  try {
    await videoRepository.clear();
  } catch (err) {
    throw new Error(`Unable to truncate 'video' table`);
  }
};

const loadAll = async () => {
  try {
    const fixtureFile = `test/video/videos.json`;
    if (fs.existsSync(fixtureFile)) {
      fixtures = JSON.parse(fs.readFileSync(fixtureFile, 'utf8'));
      await videoRepository
        .createQueryBuilder()
        .insert()
        .values(fixtures)
        .execute();
    }
  } catch (error) {
    throw new Error(`ERROR loading fixtures for video db: ${error}`);
  }
};

describe('Videos (e2e)', () => {
  let app: INestApplication;
  let videoService: VideoService;

  beforeAll(async () => {
    if (process.env.NODE_ENV !== 'test') {
      throw new Error('E2E tests are to be run in test environment');
    }

    const moduleFixture = await Test.createTestingModule({
      imports: [VideoModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    videoService = app.get<VideoService>(VideoService);
    await app.init();

    await truncateVideoTable();
    expect(await videoService.findAll()).toEqual([]);
    await loadAll();

    expect((await videoService.findAll()).length).toBe(2);
    expect(classToPlain(await videoService.findAll())).toEqual(fixtures);
  });

  describe('/videos (GET)', () => {
    it('should return all videos', async () => {
      return request(app.getHttpServer())
        .get('/videos')
        .expect(200)
        .expect(JSON.stringify(classToPlain(await videoService.findAll())));
    });
  });

  describe('/videos/{id} (GET)', () => {
    let video: Video;
    beforeAll(async () => {
      video = (await videoService.findAll())[0];
    });

    it('should return one video', async () => {
      return request(app.getHttpServer())
        .get(`/videos/${video.id}`)
        .expect(200)
        .expect(
          JSON.stringify(
            classToPlain(await videoService.findOneById(video.id)),
          ),
        );
    });
  });

  describe('/videos (POST)', () => {
    it('should create many videos', async () => {
      const data: CreateVideoDto[] = [
        {
          url: 'http://www.example.com',
          description: 'Example',
        },
        {
          url: 'http://www.domain.com',
          description: 'Domain',
        },
      ];

      await request(app.getHttpServer())
        .post('/videos')
        .send(data)
        .expect(201);

      expect((await videoService.findAll()).length).toBe(4);
    });

    afterAll(async () => {
      await app.close();
    });
  });
});
