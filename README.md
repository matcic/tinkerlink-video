## Description

REST API for video repo

## Model

Each video has the following fields:

- **id**: number;
- **url**: string;
- **description**: string;
- **createdAt**: Date;
- **updatedAt**: Date;

## Installation

```bash
$ yarn
```

## Run the app

```bash
# Spin up development environment (Docker) in watch mode
$ yarn dev

# Spin production environment (Docker)
yarn prod
```

## Run tests

```bash
# Spin up test environment (Docker) & run unit/e2e tests
$ yarn test
```

## API

Once an environment (dev | prod) is up the API is available on http://localhost:3000:

- GET http://localhost:3000/videos => get all videos
- GET http://localhost:3000/video/{videoId} => get one video
- POST http://localhost:3000/videos => create multiple videos

## Database

Postgres is available on localhost on port ${POSTGRES_PORT} (54321 by default, can be overridden in .env) and a UI to interact with it is also available on http://localhost:${ADMINER_PORT} (4000 by default, can be overridden in .env). Credentials to access the DB instance can be found in environment files according to environment (env.dev, env.prod, env.test).
