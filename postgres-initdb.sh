#!/bin/sh -e

psql --username "$POSTGRES_USER" <<-EOSQL
  CREATE DATABASE "dev";
  CREATE DATABASE "test";
  CREATE DATABASE "prod";
EOSQL

psql --username "$POSTGRES_USER" --dbname=$POSTGRES_DB <<-EOSQL
  CREATE EXTENSION "uuid-ossp";
  CREATE EXTENSION "hstore";
EOSQL