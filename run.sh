#!/bin/sh -e

# Defaults #
env="prod"
rebuild=false

# Parse Parameters #
for ARG in $*; do
  case $ARG in
    -r|--rebuild)
      rebuild=true 
      ;;
    -h|--help)
      echo "Usage: [-r|--rebuild] [-e=|--env=dev|prod|test]"
      exit 0
      ;;
    -e=*|--env=*)
      env=${ARG#*=} 
      ;;
    *)
      echo "Unknown Argument $ARG"
      echo "Usage: [-r|--rebuild] [-e=|--env=dev|prod|test]"
      exit 1
      ;;
  esac
done

if [ "$env" = "dev" ] || [ "$env" = "prod" ] || [ "$env" = "test" ]; then
  if [ "$rebuild" = true ]; then
    docker-compose -f docker-compose.yml -f "$env".yml -f docker-compose.override.yml rm -f &&
    docker-compose -f docker-compose.yml -f "$env".yml -f docker-compose.override.yml pull &&
    docker-compose -f docker-compose.yml -f "$env".yml -f docker-compose.override.yml build --no-cache &&
    docker-compose -f docker-compose.yml -f "$env".yml -f docker-compose.override.yml up --force-recreate --exit-code-from api
  else
    docker-compose -f docker-compose.yml -f "$env".yml -f docker-compose.override.yml up --exit-code-from api
  fi
else
  echo "Usage: [-r|--rebuild] [-e=|--env=dev|prod|test]"
  exit 2
fi
